#!/bin/bash

#Start Menu

ans=$(zenity --list --text "Main Menu" --radiolist --column "pick" --column "Manufacturer" TRUE "Sony" FALSE "Samsung" FALSE "LG" FALSE "Download_Sony_Firmware" FALSE "Add_Sony_Firmware" FALSE "Reinstall_All_Apps");
echo $ans
if [ $ans == "Sony" ]; then
	/bin/bash /root/Sony_Flasher/local/Zenitylocal.sh 
elif [ $ans == "Samsung" ]; then
	zenity --info --text="Flash files are located at /root/Samsung_Share"
	JOdin3CASUAL
elif [ $ans == "LG" ]; then
	zenity --info --text="Flash files are located at /root/LG_Share"
	/bin/bash /root/SALT/salt
elif [ $ans == "Download_Sony_Firmware" ]; then
	mono /root/Xperifirm/XperiFirm.exe
elif [ $ans == "Add_Sony_Firmware" ]; then
	/bin/bash /root/newsonyfirm/NewFW_v0.3.sh
elif [ $ans == "Reinstall_All_Apps" ]; then
	pss=$(zenity --password)
	if [ $pss == "21reset" ]; then
		/bin/bash /root/reinstall/reset.sh
	else
		zenity --error --text="Password wrong"
	fi

else 
	zenity --error ==text "Unsupported Device"
fi
